     
     Chapter 20 Worksheet

     

     
  1. (1 pt) Take the following program:
     
     score = 41237
     highscore = 1023407

     print("Score:      " + str(score) )
     print("High score: " + str(highscore) )
     
     Which right now outputs:
     
     Score:      41237
     High score: 1023407
     
     Use print formatting so that the output instead looks like:
     
     Score:          41,237
     High score:  1,023,407
     
     Make sure the print formatting works for any integer from zero to nine million.

  2. (3 pts) Create a program that loops from 1 to 20 and lists the decimal
     equivalent of their inverse. Use print formatting to exactly match the following
     output:
     
     1/1  = 1.0
     1/2  = 0.5
     1/3  = 0.333
     1/4  = 0.25
     1/5  = 0.2
     1/6  = 0.167
     1/7  = 0.143
     1/8  = 0.125
     1/9  = 0.111
     1/10 = 0.1
     1/11 = 0.0909
     1/12 = 0.0833
     1/13 = 0.0769
     1/14 = 0.0714
     1/15 = 0.0667
     1/16 = 0.0625
     1/17 = 0.0588
     1/18 = 0.0556
     1/19 = 0.0526
     1/20 = 0.05
     
  3. (3 pts) Write a recursive function that will calculate the Fibonacci series,
     and use output formatting. Your result should look like:

     
      1 -         0
      2 -         1
      3 -         1
      4 -         2
      5 -         3
      6 -         5
      7 -         8
      8 -        13
      9 -        21
     10 -        34
     11 -        55
     12 -        89
     13 -       144
     14 -       233
     15 -       377
     16 -       610
     17 -       987
     18 -     1,597
     19 -     2,584
     20 -     4,181
     21 -     6,765
     22 -    10,946
     23 -    17,711
     24 -    28,657
     25 -    46,368
     26 -    75,025
     27 -   121,393
     28 -   196,418
     29 -   317,811
     30 -   514,229
     31 -   832,040
     32 - 1,346,269
     33 - 2,178,309
     34 - 3,524,578
     35 - 5,702,887
     

  4. (1 pt) Why does the problem above run so slow? How could it be made to run
     faster? Ask if you aren't sure.
     
